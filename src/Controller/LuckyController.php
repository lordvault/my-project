<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LuckyController extends Controller
{
    public function number()
    {
        $number = mt_rand(0, 100);
        return $this->render('lucky/number.html.twig', array('number' => $number,));
        /*
        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
        */
    }

    public function index(SessionInterface $session)
    {
        // stores an attribute for reuse during a later user request
        $session->set('foo', 'bar');

        // gets the attribute set by another controller in another request
        $foobar = $session->get('foobar');

        // uses a default value if the attribute doesn't exist
        $filters = $session->get('filters', array());
    }
}
